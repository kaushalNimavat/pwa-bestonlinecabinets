import Vue from 'vue'
import VueGtm from 'vue-gtm'
import { Store } from 'vuex'
import { currentStoreView } from '@vue-storefront/core/lib/multistore'
import { isServer } from '@vue-storefront/core/helpers'

declare const window: any;
export const isEnabled = (gtmId: string | null) => {
  return typeof gtmId === 'string' && gtmId.length > 0 && !isServer
}

export function afterRegistration (config, store: Store<any>) {
  if (isEnabled(config.googleTagManager.id)) {
    const GTM: VueGtm = (Vue as any).gtm

    const storeView = currentStoreView()
    const currencyCode = storeView.i18n.currencyCode

    const getProduct = (item) => {
      let product = {}

      const attributeMap: string[]|Record<string, any>[] = config.googleTagManager.product_attributes
      attributeMap.forEach(attribute => {
        const isObject = typeof attribute === 'object'
        let attributeField = isObject ? Object.keys(attribute)[0] : attribute
        let attributeName = isObject ? Object.values(attribute)[0] : attribute

        if (item.hasOwnProperty(attributeField) || product.hasOwnProperty(attributeName)) {
          const value = item[attributeField] || product[attributeName]
          if (value) {
            product[attributeName] = value
          }
        }
      })

      return product
    }

    const getGAProduct = (item) => {
      let product = {}
      const attributeMap: string[]|Record<string, any>[] = config.googleTagManager.ga_product_attributes
      attributeMap.forEach(attribute => {
        const isObject = typeof attribute === 'object'
        let attributeField = isObject ? Object.keys(attribute)[0] : attribute
        let attributeName = isObject ? Object.values(attribute)[0] : attribute

        if (item.hasOwnProperty(attributeField) || product.hasOwnProperty(attributeName)) {
          const value = item[attributeField] || product[attributeName]
          if (value) {
            product[attributeName] = value
          }
        }
      })

      return product
    }

    store.subscribe(({ type, payload }, state) => {
      // According to Universal Analytics

      // Adding a Product to a Shopping Cart
      if (type === 'cart/cart/ADD') {
        GTM.trackEvent({
          event: 'addToCart',
          ecommerce: {
            currencyCode: currencyCode,
            add: {
              products: [getProduct(payload.product)]
            }
          }
        });
      }

      // Removing a Product from a Shopping Cart
      if (type === 'cart/cart/DEL') {
        GTM.trackEvent({
          event: 'removeFromCart',
          ecommerce: {
            remove: {
              products: [getProduct(payload.product)]
            }
          }
        });
      }

      // Product list
      if (type === 'google-tag-manager/productImpression') {
        const products = payload.map(product => getProduct(product))
        GTM.trackEvent({
          event: 'productImpression',
          ecommerce: {
            currencyCode: currencyCode,
            impressions: products
          }
        })
      }

      // Product Click Event
      if (type === 'google-tag-manager/productClick') {
        GTM.trackEvent({
          event: 'productClick',
          ecommerce: {
            products: [getProduct(payload)]
          }
        });
      }

      // Measuring Views of Product Details
      if (type === 'product/product/SET_CURRENT') {
        GTM.trackEvent({
          ecommerce: {
            detail: {
              'products': [getProduct(payload)]
            }
          }
        });
      }

      // Measuring Purchases
      if (type === 'order/orders/LAST_ORDER_CONFIRMATION') {
        const orderId = payload.confirmation.backendOrderId
        const products = payload.order.products.map(product => getProduct(product))
        window.localStorage.setItem('getProducts', JSON.stringify(products))
        store.dispatch(
          'user/getOrdersHistory',
          { refresh: true, useCache: false }
        ).then(() => {
          const orderHistory = state.user.orders_history
          const order = state.user.orders_history ? orderHistory.items.find((order) => order['entity_id'].toString() === orderId) : null
          GTM.trackEvent({
            'event': 'transaction',
            'ecommerce': {
              'purchase': {
                'actionField': {
                  'id': orderId,
                  'affiliation': order ? order.store_name : '',
                  'revenue': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_grand_total ? state.cart.platformTotals.base_grand_total : '',
                  'tax': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_tax_amount ? state.cart.platformTotals.base_tax_amount : '',
                  'shipping': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_shipping_amount ? state.cart.platformTotals.base_shipping_amount : ''
                },
                'products': products
              }
            }
          })
        })
      }

      // Checkout Page datalayer
      if (type === 'url/URL/SET_CURRENT_ROUTE' && payload.name.search('checkout') >= 0) {
        try {
          let step = payload.hash.slice(1)
          const checkout_products = store.getters['cart/getCartItems'].map(product => getProduct(product))
          if(!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            'event': 'checkout',
            'ecommerce': {
              'currencyCode': currencyCode,
              'checkout': {
                'actionField': { 'step': step == 'shipping' ? 2 : step == 'payment' ? 3 : step == 'orderReview' ? 4 : 1, 'option' : step != '' ? step : 'personalDetails' },
                'products': checkout_products
              }
            }
          })
        } catch (e) {
          console.log('Exception', e)
        }
      }

      if (type === 'checkout/checkout/SET_THANKYOU') {
        const products = JSON.parse(window.localStorage.getItem('getProducts'));
        GTM.trackEvent({
          'event': 'confirm-order',
          'ecommerce': {
            'confirm-order': {
              'actionField': { 'list': '' },
              'products': products
            }
          }
        })
        window.localStorage.removeItem('getProducts');
      }

      // According to GA4
      // Adding a Product to a Shopping Cart
      if (type === 'cart/cart/ADD') {
        GTM.trackEvent({
          event: 'add_to_cart',
          currency: currencyCode,
          items: [getGAProduct(payload.product)]
        });
      }

      // Removing a Product from a Shopping Cart
      if (type === 'cart/cart/DEL') {
        GTM.trackEvent({
          event: 'remove_from_cart',
          currency: currencyCode,
          items: [getGAProduct(payload.product)]
        });
      }

      // Product list view
      if (type === 'google-tag-manager/productImpression') {
        const products = payload.map(product => getGAProduct(product))
        GTM.trackEvent({
          event: 'view_item_list',
          ecommerce: {
            items: products
          }
        })
      }

      // Product Click Event
      if (type === 'google-tag-manager/productClick') {
        GTM.trackEvent({
          event: 'select_item',
          items: {
            products: [getGAProduct(payload)]
          },
          currency: currencyCode,
        });
      }

      // View current product
      if (type === 'product/product/SET_CURRENT') {
        GTM.trackEvent({
          event: 'view_item',
          currency: currencyCode,
          items: [getGAProduct(payload)]
        });
      }

      // View checkout page with steps
      if (type === 'url/URL/SET_CURRENT_ROUTE' && payload.name.search('checkout') >= 0) {
        try {
          let step = payload.hash.slice(1)
          const checkout_products = store.getters['cart/getCartItems'].map(product => getGAProduct(product))
          let cartTotal = store.getters['cart/getTotals'];
          cartTotal = cartTotal.find(cart => cart.code === 'grand_total')
          if(!isServer) {
            window.dataLayer.push({ ecommerce: null })
          }
          GTM.trackEvent({
            'event': step == 'shipping' ? 'add_shipping_info' : step == 'payment' ? 'add_payment_info' : step == 'orderReview' ? 'add_order_review_info' : 'add_personal_info', 'option' : step != '' ? step : 'add_personal_info',
            'ecommerce': {
              'value': cartTotal ? cartTotal.value : 0,
              'currency': currencyCode,
              'items': checkout_products
            }
          })
        } catch (e) {
          console.log('Exception', e)
        }
      }

      // Event after place order
      if (type === 'order/orders/LAST_ORDER_CONFIRMATION') {
        const orderId = payload.confirmation.backendOrderId
        const products = payload.order.products.map(product => getGAProduct(product))
        window.localStorage.setItem('getGAProducts', JSON.stringify(products))
        store.dispatch(
          'user/getOrdersHistory',
          { refresh: true, useCache: false }
        ).then(() => {
          const orderHistory = state.user.orders_history
          const order = state.user.orders_history ? orderHistory.items.find((order) => order['entity_id'].toString() === orderId) : null
          GTM.trackEvent({
            'event': 'purchase',
            'ecommerce': {
              'transaction_id': orderId,
              'currency': currencyCode,
              'tax': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_tax_amount ? state.cart.platformTotals.base_tax_amount : '',
              'shipping': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_shipping_amount ? state.cart.platformTotals.base_shipping_amount : '',
              'value': order ? order.total_due : state.cart.platformTotals && state.cart.platformTotals.base_grand_total ? state.cart.platformTotals.base_grand_total : '',
              'items': products
            }
          })
        })
      }

      // Event for thank you page to confirm order
      if (type === 'checkout/checkout/SET_THANKYOU') {
        const products = JSON.parse(window.localStorage.getItem('getGAProducts'));
        GTM.trackEvent({
          'event': 'confirm-order',
          'ecommerce': {
            'confirm-order': {
              'actionField': { 'list': '' },
              'products': products
            }
          }
        })
        window.localStorage.removeItem('getGAProducts');
      }
    })
  }
}
