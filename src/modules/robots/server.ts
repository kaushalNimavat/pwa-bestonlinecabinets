import { serverHooks } from '@vue-storefront/core/server/hooks'
import fetch from 'isomorphic-fetch'
const config = require('config')

serverHooks.afterApplicationInitialized(({ app }) => {
  app.get('/robots.txt', (req, res) => {
    res.end('User-agent: *\nDisallow: ')
  })
})
