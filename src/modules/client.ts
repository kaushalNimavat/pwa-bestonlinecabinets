import { VueStorefrontModule } from '@vue-storefront/core/lib/module'
import { CatalogModule } from '@vue-storefront/core/modules/catalog'
import { CatalogNextModule } from '@vue-storefront/core/modules/catalog-next'
import { CartModule } from '@vue-storefront/core/modules/cart'
import { CheckoutModule } from '@vue-storefront/core/modules/checkout'
import { CompareModule } from '@vue-storefront/core/modules/compare'
import { WishlistModule } from '@vue-storefront/core/modules/wishlist'
import { NotificationModule } from '@vue-storefront/core/modules/notification'
import { UrlModule } from '@vue-storefront/core/modules/url'
import { BreadcrumbsModule } from '@vue-storefront/core/modules/breadcrumbs'
import { UserModule } from '@vue-storefront/core/modules/user'
import { CmsModule } from '@vue-storefront/core/modules/cms'
import { GoogleTagManagerModule } from './google-tag-manager';
// import { AmpRendererModule } from './amp-renderer';
import { PaymentBackendMethodsModule } from './payment-backend-methods'
import { PaymentCashOnDeliveryModule } from './payment-cash-on-delivery'
import { NewsletterModule } from '@vue-storefront/core/modules/newsletter'
import { InitialResourcesModule } from '@vue-storefront/core/modules/initial-resources'
import { BocmproductModule } from './bocm/bocm-products'
import { CurrentSaleModule } from './bocm/current-sale'
import { BannerModule } from './bocm/banners'
import { AssemblyModule } from './bocm/assembly'
import { BocmCategoryModule } from './bocm/bocm-category'
import { FreshchatModule } from './bocm/freshchat'
import { CategoryCompareModule } from './bocm/bocm-compare'
// import { DeviceModule } from './device/index';
import { registerModule } from '@vue-storefront/core/lib/modules'
import { addressBook } from './bocm/addres-book'
import { ProductQuestionModule } from './bocm/product-question'
import { StorePaymentModule } from './bocm/storePayment'
import { CustomWishlistModule } from './bocm/custom-wishlist'
import { Braintree } from './bocm/vsf-payment-braintree';
import { GoogleRecaptcha } from './bocm/google-recaptcha'
import { SocialAuthModule } from './bocm/social-auth'
import { AffirmModule } from './bocm/affirm'
import { FaqModule } from './bocm/faq'
import { ResearchModule } from './bocm/research'
// TODO:distributed across proper pages BEFORE 1.11
export function registerClientModules () {
  registerModule(UrlModule)
  registerModule(CatalogModule)
  registerModule(CheckoutModule) // To Checkout
  registerModule(CartModule)
  registerModule(PaymentBackendMethodsModule)
  registerModule(PaymentCashOnDeliveryModule)
  registerModule(WishlistModule) // Trigger on wishlist icon click
  registerModule(NotificationModule)
  registerModule(UserModule) // Trigger on user icon click
  registerModule(CatalogNextModule)
  registerModule(CompareModule)
  registerModule(BreadcrumbsModule)
  registerModule(GoogleTagManagerModule)
  // registerModule(AmpRendererModule)
  registerModule(CmsModule)
  registerModule(NewsletterModule)
  registerModule(InitialResourcesModule)
  registerModule(BocmproductModule)
  registerModule(CurrentSaleModule)
  // registerModule(DeviceModule)
  registerModule(BannerModule)
  registerModule(AssemblyModule)
  registerModule(BocmCategoryModule)
  registerModule(FreshchatModule)
  registerModule(CategoryCompareModule)
  registerModule(addressBook)
  registerModule(ProductQuestionModule)
  registerModule(StorePaymentModule)
  registerModule(CustomWishlistModule)
  registerModule(Braintree)
  registerModule(AffirmModule)
  registerModule(FaqModule)
  registerModule(ResearchModule)
  registerModule(SocialAuthModule)
}

// Deprecated API, will be removed in 2.0
export const registerModules: VueStorefrontModule[] = [
  GoogleRecaptcha
  // Example
]
