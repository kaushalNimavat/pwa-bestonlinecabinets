import { ActionTree } from 'vuex';
import ProductQuestionState from '../types/ProductQuestionState';
import rootStore from '@vue-storefront/core/store'
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'
import config from 'config'
import i18n from '@vue-storefront/i18n'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

const entityName = 'faq'
const actions: ActionTree<ProductQuestionState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (context, { product = null, sort = null, filters = {}, skipCache = false }) {
    let query = new SearchQuery()
    query = query.applyFilter({ key: 'product_ids', value: { 'in': product.id } })
    query = query.applyFilter({ key: 'status', value: { 'eq': 1 } })
    if (skipCache || !context.state.items || context.state.items.length === 0) {
      return quickSearchByQuery({
        query,
        entityType: entityName,
        sort: 'position:asc'
      })
        .then(resp => {
          context.commit(types.FETCH_ITEMS, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'Product Question')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.items;
        resolve(resp);
      });
    }
  },

  async add (context, contactData: ProductQuestionState) {
    let url = processLocalizedURLAddress(config.bocm.productQuestion.endpoint)
    try {
      return await fetch(url, {
        method: 'POST',
        headers: {
          'Accept': 'application/json, text/plain, */*',
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(contactData)
      }).then(res => res.json()).then(res => res)
    } catch (e) {
      rootStore.dispatch('notification/spawnNotification', {
        type: 'error',
        message: i18n.t('Something went wrong. Try again in a few seconds.'),
        action1: { label: i18n.t('OK') }
      })
    };
  }
};
export default actions;
