import ProductQuestionState from '../types/ProductQuestionState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<ProductQuestionState, any> = {
  getProductQuestionList: (state) => state.items
}
