import { Module } from 'vuex'
import ProductQuestionState from '../types/ProductQuestionState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const ProductQuestionModuleStore: Module<ProductQuestionState, any> = {
  namespaced: true,
  state: {
    items: []
  },
  mutations,
  actions,
  getters
}
