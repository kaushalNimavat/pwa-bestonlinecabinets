import { Module } from 'vuex'
import AssemblyState from '../types/AssemblyState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const AssemblyModuleStore: Module<AssemblyState, any> = {
  namespaced: true,
  state: {
    items: [],
    high_gloss_items: [],
    textured_items: []
  },
  mutations,
  actions,
  getters
}
