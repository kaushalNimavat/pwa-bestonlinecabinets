import { ActionTree } from 'vuex';
import AssemblyState from '../types/AssemblyState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder';

const AssemblyEntityName = 'flatpanel';
const actions: ActionTree<AssemblyState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */
  list (
    context, {
      skipCache = false,
      filterValues = null, 
      filterField = null
    }
  ) {
    let query = new SearchQuery();
    if (
      skipCache ||
      !context.state.items ||
      context.state.items.length === 0
    ) {
      if (filterValues) {
        query = query.applyFilter({ key: filterField, value: { in: filterValues }})
      }
      return quickSearchByQuery({
        query,
        entityType: AssemblyEntityName
      })
        .then(resp => {
          if (!filterValues) {
            context.commit(types.FETCH_ITEMS, resp.items);
          } else {
            let highGlossData = resp.items.filter(item =>{
              return item.flat_panel.id === 1 ? item: false
            })
            let texturedData= resp.items.filter(item =>{
              return item.flat_panel.id === 2 ? item: false
            })
            context.commit(types.FETCH_HIGH_GLOSS_ITEMS, highGlossData);
            context.commit(types.FETCH_TEXTURED_ITEMS, texturedData);
          }
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'Assembly Instructions')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.items;
        resolve(resp);
      });
    }
  }
};
export default actions;
