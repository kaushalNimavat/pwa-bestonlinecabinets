import AssemblyState from '../types/AssemblyState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<AssemblyState, any> = {
  getAssemblyList: (state) => state.items,
  getHighGlossFlatPanel:(state) => state.high_gloss_items,
  getTexturedFinishFlatPanel:(state) => state.textured_items
}
