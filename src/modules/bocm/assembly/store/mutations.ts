import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_ITEMS] (state, items) {
    state.items = items || []
  },
  [types.FETCH_HIGH_GLOSS_ITEMS] (state, items) {
    state.high_gloss_items = items || []
  },
  [types.FETCH_TEXTURED_ITEMS] (state, items) {
    state.textured_items = items || []
  }
}
