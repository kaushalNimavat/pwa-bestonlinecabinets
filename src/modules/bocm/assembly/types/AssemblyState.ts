export default interface AssemblyState {
  items: any[]
  high_gloss_items: any[]
  textured_items: any[]
}
