import Vue from 'vue';
import { Store } from 'vuex';
import { isServer } from '@vue-storefront/core/helpers'

declare const window: any;

const googleAuth = (function () {
  function installGoogleClient () {
    var apiUrl = 'https://apis.google.com/js/api.js';
    return new Promise<void>(resolve => {
      var script = document.createElement('script') as any;
      script.src = apiUrl;
      script.onreadystatechange = script.onload = function () {
        if (!script.readyState || /loaded|complete/.test(script.readyState)) {
          setTimeout(() => {
            resolve();
          }, 500);
        }
      };
      document.getElementsByTagName('head')[0].appendChild(script);
    });
  }

  function initGoogleClient (config) {
    return new Promise(resolve => {
      let k = window || this;

      k.gapi.load('auth2', () => {
        k.gapi.auth2.init(config).then(() => {
          resolve(k.gapi);
        });
      });
    });
  }

  function Auth () {
    if (!(this instanceof Auth)) {
      return new (Auth as any)();
    }
    this.GoogleAuth = null;
    this.isAuthorized = false;
    this.isInit = false;
    this.prompt = null;
    this.isLoaded = function () {
      /* eslint-disable */
      console.warn(
        'isLoaded() will be deprecated. You can use "this.$gAuth.isInit"'
      );
      return !!this.GoogleAuth;
    };

    this.load = (config, prompt) => {
      if(!isServer) {
      installGoogleClient()
        .then(() => {
          return initGoogleClient(config);
        })
        .then(gapi => {
          if (!gapi) {
            let w = window || this;
            gapi = w.gapi;
          }

          if (gapi.hasOwnProperty("config")) {
            this.GoogleAuth = gapi["auth2"].getAuthInstance();
            this.isInit = true;
            this.prompt = prompt;
            this.isAuthorized = this.GoogleAuth.isSignedIn.get();
          }
        });
      }
    };
    this.signIn = (successCallback, errorCallback) => {
      return new Promise((resolve, reject) => {
        console.log("Google Auth", this.GoogleAuth);
        if (!this.GoogleAuth) {
          if (typeof errorCallback === "function") errorCallback(false);
          reject(false);
          return;
        }
        this.GoogleAuth.signIn()
          .then(googleUser => {
            if (typeof successCallback === "function") {
              successCallback(googleUser);
            }
            this.isAuthorized = this.GoogleAuth.isSignedIn.get();
            resolve(googleUser);
          })
          .catch(error => {
            console.log(error);
            if (typeof errorCallback === "function") errorCallback(error);
            reject(error);
          });
      });
    };
    this.getAuthCode = (successCallback, errorCallback) => {
      return new Promise((resolve, reject) => {
        if (!this.GoogleAuth) {
          if (typeof errorCallback === "function") errorCallback(false);
          reject(false);
          return;
        }
        this.GoogleAuth.grantOfflineAccess({ prompt: this.prompt })
          .then(resp => {
            if (typeof successCallback === "function") {
              successCallback(resp.code);
            }
            resolve(resp.code);
          })
          .catch(error => {
            if (typeof errorCallback === "function") errorCallback(error);
            reject(error);
          });
      });
    };

    this.signOut = (successCallback, errorCallback) => {
      return new Promise((resolve, reject) => {
        if (!this.GoogleAuth) {
          if (typeof errorCallback === "function") errorCallback(false);
          reject(false);
          return;
        }
        this.GoogleAuth.signOut()
          .then(() => {
            if (typeof successCallback === "function") successCallback();
            this.isAuthorized = false;
            resolve(true);
          })
          .catch(error => {
            if (typeof errorCallback === "function") errorCallback(error);
            reject(error);
          });
      });
    };
  }
  return Auth();
})();
function installGoogleAuthPlugin(Vue, options) {
  // set config
  let GoogleAuthConfig = null;
  let GoogleAuthDefaultConfig = {
    scope: "profile email",
    discoveryDocs: [
      "https://www.googleapis.com/discovery/v1/apis/drive/v3/rest"
    ]
  };
  let prompt = "select_account";
  if (typeof options === "object") {
    GoogleAuthConfig = Object.assign(GoogleAuthDefaultConfig, options);
    if (options.scope) GoogleAuthConfig.scope = options.scope;
    if (options.prompt) prompt = options.prompt;
    if (!options.clientId) {
      console.warn("clientId is required");
    }
  } else {
    console.warn("invalid option type. Object type accepted only");
  }
  // Install Vue plugin
  Vue.gAuth = googleAuth;
  if (typeof Vue.prototype.$gAuth === "undefined") {
    Vue.prototype.$gAuth = {};
  }
  Vue.prototype.$gAuth = Vue.gAuth;

  Vue.gAuth.load(GoogleAuthConfig, prompt);
}
function initFacebook() {
  window.fbAsyncInit = function() {
    window.FB.init({
      appId: '484532279591704',
      cookie: true,
      version: "v12.0"
    });
  };
}
function loadFacebookSDK(d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = "https://connect.facebook.net/en_US/sdk.js";
  fjs.parentNode.insertBefore(js, fjs);
}
function loadAmazonSDK(d, s, id) {
  var js,
    fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) {
    return;
  }
  js = d.createElement(s);
  js.id = id;
  js.src = "https://assets.loginwithamazon.com/sdk/na/login1.js";
  fjs.parentNode.insertBefore(js, fjs);
  window.onAmazonLoginReady = function () {
    // eslint-disable-next-line no-undef
    window.amazon.Login.setClientId('amzn1.application-oa2-client.b396b9fe73a146348b1973247935b0af');
  };
}
export function afterRegistration(config, store: Store<any>) {
  const gauthOption = {
    clientId: "719476270682-k5jish1s13lpgm7ijqarnlsuhn62uoqp.apps.googleusercontent.com",
    scope: "profile email",
    prompt: "select_account"
  };
  setTimeout(() => {
    Vue.use(installGoogleAuthPlugin, gauthOption);
    if (typeof document !== "undefined") {
      /* register facebook auth */
      loadFacebookSDK(document, "script", "facebook-jssdk");
      initFacebook();
      /* register amazon auth*/
      loadAmazonSDK(document, "script", "amazon-login-sdk");
     }
  }, 1000);
  Vue.config.productionTip = false;
}
