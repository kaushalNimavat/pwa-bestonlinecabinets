
import { StorefrontModule } from '@vue-storefront/core/lib/modules'
import { beforeRegistration } from './hooks/beforeRegistration'
import { AffirmModuleStore } from './store/index';
export const KEY = 'affirm'

export const AffirmModule: StorefrontModule = function ({ store, router, appConfig }) {
	beforeRegistration(store, appConfig)
   store.registerModule(KEY, AffirmModuleStore)
}
