import { ActionTree } from 'vuex';
import AffirmState from '../types/AffirmState';
import { Logger } from '@vue-storefront/core/lib/logger'
import fetch from 'isomorphic-fetch'
import rootStore from '@vue-storefront/core/store'
import { adjustMultistoreApiUrl } from '@vue-storefront/core/lib/multistore'
import { processLocalizedURLAddress } from '@vue-storefront/core/helpers';

import { router } from '@vue-storefront/core/app'

const actions: ActionTree<AffirmState, any> = {
  async affirmAuthorize({ dispatch, commit }, affirmPaymentData) {
    let orderData = await dispatch('fetchOrderData');
    orderData = JSON.parse(orderData)
    if (orderData.confirmation) {
      try {
        let url = rootStore.state.config.api.url + '/api/ext/boc/affirm/authorize'
        if (rootStore.state.config.storeViews.multistore) {
          url = adjustMultistoreApiUrl(url)
        }
        url = processLocalizedURLAddress(url);
        Logger.info('Magento 2 REST API Request with Request Data', 'Affirm')()
        await fetch(url, {
          method: 'POST',
          headers: {
            'Accept': 'application/json, text/plain, */*',
            'Content-Type': 'application/json'
          }, body: JSON.stringify({ affirmPaymentData: affirmPaymentData, orderData: orderData })
        })
          .then(response => response.json())
          .then(data => {
            if (data.code === 200) {
              router.push('/thank-you')
            } else {
              router.push('/thank-you?error=order-canceled')
            }
          })
      } catch (e) {
        Logger.error(e, 'Affirm')()
      }
    }
  },
  async fetchOrderData() {
    return localStorage.getItem('affirm-order')
  },
};
export default actions;
