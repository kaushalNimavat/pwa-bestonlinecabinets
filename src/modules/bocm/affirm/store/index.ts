import { Module } from 'vuex'
import AffirmState from '../types/AffirmState'
import actions from './actions'
export const AffirmModuleStore: Module<AffirmState, any> = {
  namespaced: true,
  actions,
}
