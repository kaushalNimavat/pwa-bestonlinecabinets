export const RemoveAddress = {
  name: 'RemoveAddress',
  methods: {
    removeAddress (addressId) {
      return new Promise((resolve) => {
        this.$store.dispatch('address-book/removeAddress', addressId)
        // resolve()
      })
    }
  }
}
