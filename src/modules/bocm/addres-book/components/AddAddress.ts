import Address from '../types/Address'

export const AddAddress = {
  name: 'AddAddress',
  methods: {
    addAddress (address: Address) {
      return new Promise(() => {
        this.$store.dispatch('address-book/addAddress', address)
        // resolve()
      })
    },
    updateAddress (currentAddressId, address: Address) {
      return new Promise(() => {
        this.$store.dispatch('address-book/updateAddress', { 'currentAddressId': currentAddressId, 'newAddress': address })
        // resolve()
      })
    }
  }
}
