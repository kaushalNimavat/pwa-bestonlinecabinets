import { ActionTree } from 'vuex';
import BocmproductState from '../types/BocmproductState';
import * as types from './mutation-types';
import rootStore from '@vue-storefront/core/store'
import { baseFilterProductsQuery } from '@vue-storefront/core/helpers'
import config from 'config';

const actions: ActionTree<BocmproductState, any> = {
  async categoryProductList ({ getters, commit }, category) {
    const query = baseFilterProductsQuery(category)
    const { items } = await rootStore.dispatch('product/findProducts', {
      query: query,
      options: {
        populateRequestCacheTags: false,
        prefetchGroupProducts: false
      }
    }, { root: true })
    let products = getters.getCategoryProductList
    let obj = products.find(o => o.categoryId === category.id);
    if (typeof obj === 'undefined') {
      rootStore.commit('google-tag-manager/productImpression', items)
      commit(types.FETCH_BOCM_CATEGORY_PRODUCT_LIST, { categoryId: category.id, products: items })
    }
  }
};
export default actions;
