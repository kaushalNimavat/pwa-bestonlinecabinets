import { Module } from 'vuex'
import BocmproductState from '../types/BocmproductState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const BocmproductModuleStore: Module<BocmproductState, any> = {
  namespaced: true,
  state: {
    products: []
  },
  mutations,
  actions,
  getters
}
