import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_BOCM_CATEGORY_PRODUCT_LIST] (state, categoryData) {
    state.products.push(categoryData)
  }
}
