import { Module } from 'vuex'
import FaqState from '../types/FaqState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const FaqModuleStore: Module<FaqState, any> = {
  namespaced: true,
  state: {
    faq: []
  },
  mutations,
  actions,
  getters
}
