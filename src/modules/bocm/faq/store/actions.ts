import { ActionTree } from 'vuex';
import FaqState from '../types/FaqState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'

import _find from 'lodash-es/find'
import _filter from 'lodash-es/filter'
import _flatMap from 'lodash-es/flatMap'
/* eslint-disable */
import _ from 'lodash-es'

const entityName = 'faq'
const actions: ActionTree<FaqState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (context, { filterValues = null, filterField = null, skipCache = false }) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { eq: filterValues }
      });
    }
    if (skipCache || !context.state.faq || context.state.faq.length === 0) {
      return quickSearchByQuery({
        query,
        entityType: entityName
      })
        .then(resp => {
          context.commit(types.FETCH_FAQ, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'Faq')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.faq;
        resolve(resp);
      });
    }
  }
};
export default actions;
