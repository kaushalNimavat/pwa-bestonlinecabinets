import FaqState from '../types/FaqState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<FaqState, any> = {
  getFaqList: (state) => state.faq,
  getFAQCategoryTitle: (state) => {
    if (state.faq.length) {
      let catTitle = []
      state.faq.map(faq => {
        if (faq.category) {
          faq.category.map(cat => catTitle.push({ id: cat.category_id, title: cat.title }))
        } else {
          // TODO: List of not having category field
        }
      })
      return [...new Map(catTitle.map(v => [v.id, v])).values()] // remove duplicate category object
    } else {
      return []
    }
  },
  filteredFAQ: (state, getters) => {
    const filteredTitle = getters.getFAQCategoryTitle
    let titledFAQ = []
    if (filteredTitle.length) {
      filteredTitle.map(category => {
        let filterFAQ = state.faq.filter(f => f.category_ids && f.category_ids.includes(category.id))
        titledFAQ.push({
          title: category.title,
          faqs: filterFAQ
        })
      })
    }
    return titledFAQ
  }
}
