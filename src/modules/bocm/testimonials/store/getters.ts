import TestimonialState from '../types/TestimonialState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<TestimonialState, any> = {
  getTestimonialsList: (state) => state.testimonials
}
