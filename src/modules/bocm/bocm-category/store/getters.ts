import BocmCategoryState from '../types/BocmCategoryState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<BocmCategoryState, any> = {
  getSubcategory: (state, getters) => {
    return state.subCategory || []
  },
  getAvailableFilters: (state, getters) => {
    return state.filters || []
  }
}
