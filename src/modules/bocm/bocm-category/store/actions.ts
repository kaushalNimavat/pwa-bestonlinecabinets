import { ActionTree } from 'vuex';
import BrandProductState from '../types/BocmCategoryState';
import { SearchQuery } from 'storefront-query-builder'
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import * as types from '../store/mutation-types'
import { Logger } from '@vue-storefront/core/lib/logger';
import config from 'config'

const actions: ActionTree<BrandProductState, any> = {
  async list ({ commit, state, dispatch }, { category = null, sort = null, filters = {} }) {
    let searchQuery = new SearchQuery()
    searchQuery = searchQuery.applyFilter({ key: 'parent_id', value: { 'eq': category.id } })
    searchQuery = searchQuery.applyFilter({ key: 'is_active', value: { 'eq': true } })

    for (var filter in filters) {
      if (filters[filter].length) {
        searchQuery = searchQuery.applyFilter({ key: filter, value: { 'in': filters[filter] } })
      }
    }

    return quickSearchByQuery({
      entityType: 'category',
      query: searchQuery,
      sort: sort ? sort.id : ''
    }).then(resp => {
      commit(types.SET_BOCM_SUB_CATEGORY, resp.items)
      return resp.items;
    })
      .catch(err => {
        Logger.error(err, 'banner')();
      });
  },
  async categoryAttributeList ({ commit }) {
    let searchQuery = new SearchQuery()
    searchQuery = searchQuery.applyFilter({ key: 'attribute_code', value: { 'in': config.entities.category.filters } })
    await quickSearchByQuery({
      entityType: 'category_attributes',
      query: searchQuery
    }).then(resp => {
      commit(types.FETCH_CATEGORY_FILTERS, resp.items)
    })
  }

};
export default actions;
