import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { CurrentSaleModuleStore } from './store/index';

export const CurrentSaleModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('current-sale', CurrentSaleModuleStore)
};
