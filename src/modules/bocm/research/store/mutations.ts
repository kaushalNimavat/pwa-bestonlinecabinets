import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_RESEARCH] (state, research) {
    state.research = research || []
  }
}
