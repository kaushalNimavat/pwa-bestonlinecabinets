import ResearchState from '../types/ResearchState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<ResearchState, any> = {
  getResearchList: (state) => state.research,
  uniqueCategory: (state, getters) => [...new Map(getters.getResearchList.flatMap(x => x.categories).map(item => [item['name'], item])).values()]
}
