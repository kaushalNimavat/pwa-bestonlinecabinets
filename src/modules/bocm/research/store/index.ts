import { Module } from 'vuex'
import ResearchState from '../types/ResearchState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const ResearchModuleStore: Module<ResearchState, any> = {
  namespaced: true,
  state: {
    research: []
  },
  mutations,
  actions,
  getters
}
