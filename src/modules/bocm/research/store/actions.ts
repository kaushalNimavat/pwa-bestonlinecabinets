import { ActionTree } from 'vuex';
import ResearchState from '../types/ResearchState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
import { SearchQuery } from 'storefront-query-builder'

const entityName = 'blogs'
const actions: ActionTree<ResearchState, any> = {
  /**
   * Retrieve groups
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (context, { filterValues = null, filterField = null, skipCache = false }) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { eq: filterValues }
      });
    }
    if (skipCache || !context.state.research || context.state.research.length === 0) {
      return quickSearchByQuery({
        query,
        entityType: entityName
      })
        .then(resp => {
          context.commit(types.FETCH_RESEARCH, resp.items);
          return resp.items;
        })
        .catch(err => {
          Logger.error(err, 'research')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.research;
        resolve(resp);
      });
    }
  }
};
export default actions;
