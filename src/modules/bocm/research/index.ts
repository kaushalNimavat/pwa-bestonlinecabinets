import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { ResearchModuleStore } from './store/index';

export const ResearchModule: StorefrontModule = function ({
  app,
  store,
  router,
  moduleConfig,
  appConfig
}) {
  store.registerModule('research', ResearchModuleStore)
};
