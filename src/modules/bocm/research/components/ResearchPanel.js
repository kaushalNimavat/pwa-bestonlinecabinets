import { mapGetters } from 'vuex'
import { getPostUrl } from 'theme/helpers';
import moment from 'moment'

export default {
  name: 'ResearchPanel',
  data () {
    return {
      search: ''
    }
  },
  computed: {
    ...mapGetters({
      getResearchList: 'research/getResearchList',
      filteredCategory: 'research/filteredCategory',
      uniqueCategory: 'research/uniqueCategory'
    }),
    featuredPost () {
      let featured = this.getResearchList.filter(f => f.is_featured === '1')
      return featured && featured.length > 0 ? featured.slice(0, 3) : [];
    },
    recentPost () {
      let featured = this.getResearchList.filter(f => f.published_at)
      return featured && featured.length > 0 ? featured.reverse().slice(0, 5) : [];
    },
    researchFilterData () {
      if (this.search) {
        return this.getResearchList.filter((item) => {
          return this.search.toLowerCase().split(' ').every(v => item.title.toLowerCase().includes(v))
        })
      } else {
        return this.getResearchList;
      }
    },
    filterCategory () {
      return this.getResearchList.filter(f => f.categories.find(c => c.url_key === this.$route.params.title));
    },
    researchFilterCategoryData () {
      if (this.search) {
        return this.filterCategory.filter((item) => {
          return this.search.toLowerCase().split(' ').every(v => item.title.toLowerCase().includes(v))
        })
      } else {
        return this.filterCategory
      }
    },
    getCategoryTitle () {
      return this.$route.params.title.replace(/[-]/g, ' ')
    }
  },
  async asyncData ({ store, route, context }) { // this is for SSR purposes to prefetch data
    await Promise.all([
      store.dispatch('research/list', { filterField: 'status' })
    ])
  },
  methods: {
    getPostImage (path) {
      return getPostUrl(path);
    },
    getFormatDate (date) {
      return moment(String(date)).format('MMMM DD, YYYY')
    }
  }
}
