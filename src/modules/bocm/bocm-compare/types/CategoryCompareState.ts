export default interface CategoryCompareState {
  /**
   * Informs if items to compare are already loaded from local cache.
   */
  loaded: boolean,
  items: any[],
  compareAttributeList: any[]
}
