import { Module } from 'vuex'
import { BraintreeState } from '../types/BraintreeState'
import { mutations } from './mutations'
import { getters } from './getters'

export const module: Module<BraintreeState, any> = {
  namespaced: true,
  state: {
    hostedFieldInstance: null
  },
  mutations,
  getters
}
