import { Module } from 'vuex'
import StorePaymentState from '../types/storePaymentState'
import actions from './actions'
import { mutations } from './mutations'
import { getters } from './getters'
export const StorePaymentModuleStore: Module<StorePaymentState, any> = {
  namespaced: true,
  state: {
    paymentMethods: []
  },
  mutations,
  actions,
  getters
}
