import StorePaymentState from '../types/storePaymentState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<StorePaymentState, any> = {
  getStoredPaymentMethodList: (state) => state.paymentMethods
}
