import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.FETCH_PAYMENT_METHOD_LIST] (state, paymentMethodList) {
    state.paymentMethods = paymentMethodList || []
  }
}
