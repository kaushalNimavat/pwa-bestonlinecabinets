import { ActionTree } from 'vuex';
import BannerState from '../../types/BannerState';
import * as types from './mutation-types';
import { Logger } from '@vue-storefront/core/lib/logger';
import { quickSearchByQuery } from '@vue-storefront/core/lib/search';
// import SearchQuery from '@vue-storefront/core/lib/search/searchQuery';
import { SearchQuery } from 'storefront-query-builder'

const actions: ActionTree<BannerState, any> = {
  /**
   * Retrieve banners
   *
   * @param context
   * @param {any} filterValues
   * @param {any} filterField
   * @param {any} size
   * @param {any} start
   * @param {any} excludeFields
   * @param {any} includeFields
   * @returns {Promise<T> & Promise<any>}
   */

  list (
    context,
    {
      filterValues = null,
      filterField = 'id',
      size = 150,
      start = 0,
      excludeFields = null,
      includeFields = null,
      skipCache = false
    }
  ) {
    let query = new SearchQuery();
    if (filterValues) {
      query = query.applyFilter({
        key: filterField,
        value: { like: filterValues }
      });
    }
    if (
      skipCache ||
      !context.state.banners ||
      context.state.banners.length === 0
    ) {
      return quickSearchByQuery({
        query,
        entityType: 'banner',
        excludeFields,
        includeFields
      })
        .then(resp => {
          let banners = resp.items.sort((a, b) => { return parseInt(a.sort_order) - parseInt(b.sort_order) })
          context.commit(types.BANNER_FETCH_BANNER, banners);
          return banners;
        })
        .catch(err => {
          Logger.error(err, 'banner')();
        });
    } else {
      return new Promise((resolve, reject) => {
        let resp = context.state.banners;
        resolve(resp);
      });
    }
  }
};
export default actions;
