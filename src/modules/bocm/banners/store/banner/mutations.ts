import { MutationTree } from 'vuex'
import * as types from './mutation-types'

export const mutations: MutationTree<any> = {
  [types.BANNER_FETCH_BANNER] (state, banners) {
    state.banners = banners || []
  }
}
