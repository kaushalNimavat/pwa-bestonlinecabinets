import CustomWishlistState from '../types/CustomWishlistState'
import { GetterTree } from 'vuex';

export const getters: GetterTree<CustomWishlistState, any> = {
  getCustomWishlistData: (state) => state.customwishlist,
  getWishlistDataList: (state) => state.wishlistdatalist
}
