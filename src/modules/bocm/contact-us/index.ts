import { StorefrontModule } from '@vue-storefront/core/lib/modules';
import { contactUsModule } from './store'

export const KEY = 'contact-us'

export const ContactUsModule: StorefrontModule = function ({ store }) {
  store.registerModule(KEY, contactUsModule)
}
