export default interface ShippingAddress {
  firstname: string,
  lastname: string,
  region_id: any,
  city: string,
  postcode: string,
  street: string[]
}
